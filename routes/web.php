<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home.index');

// News
Route::get('tin-tuc', 'NewsController@index')->name('news.index');
Route::get('tin-tuc/chi-tiet/{slug}.{id}.html', 'NewsController@detail')->name('news.detail');

// Unit
Route::get('phong-ban', 'UnitController@index')->name('unit.index');
Route::get('phong-ban/chi-tiet/{id}.html', 'UnitController@detail')->name('unit.detail');

// Staff
Route::get('can-bo', 'StaffController@index')->name('staff.index');
Route::get('can-bo/chi-tiet/{slug}.{id}.html', 'StaffController@detail')->name('staff.detail');

// Contact
Route::get('lien-he', 'ContactController@index')->name('contact.index');
Route::post('lien-he', 'ContactController@postContact')->name('contact.post');

// QUY TRÌnh THỦ TỤc
Route::get('quy-trinh-thu-tuc', 'ProcedureController@index')->name('procedure.index');