@extends('layouts.basic')
@section('content')
<section class="projects_area p_120">
    <div class="container">
        <div class="main_title">
            <h2>Cập nhật thông tin cán bộ</h2>
            <p>Xây dựng bộ máy cán bộ thực hiện nghiêm chỉnh chấp hành vì nước, vì dân</p>
        </div>
        <div class="projects_inner row">
            @foreach($staff as $item)
            <div class="col-lg-4 col-sm-6 brand web">
                <div class="projects_item">
                    <img class="img-fluid" src="http://127.0.0.1:8000/images/staff/{{$item->thumbnails}}" alt="">
                    <div class="projects_text">
                        <a href="{{route('staff.detail', ['slug' => $item->slug, 'id' => $item->id])}}"><h4>{{$item->first_name}} {{$item->last_name}}</h4></a>
                        <p>{{$item->code_number_staff}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection