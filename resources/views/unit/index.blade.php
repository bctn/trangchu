@extends('layouts.basic')
@section('content')
<section class="projects_area p_120">
    <div class="container">
        <div class="main_title">
            <h2>Cập nhật danh sách phòng ban</h2>
            <p>Xây dựng bộ máy cán bộ thực hiện nghiêm chỉnh chấp hành vì nước, vì dân</p>
        </div>
        <div class="projects_inner row">
            @foreach($unit as $item)
            <div class="col-lg-4 col-sm-6 brand web">
                <div class="">
                    <div class="projects_text">
                        <a href="{{route('unit.detail', ['id' => $item->code_number_unit])}}"><h4>{{$item->name}}</h4></a>
                        <p>Mã: {{$item->code_number_unit}}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection