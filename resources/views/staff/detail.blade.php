@extends('layouts.basic')
@section('content')
    <section class="home_banner_area">
        <div class="banner_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="home_left_img">
                            <img src="http://127.0.0.1:8000/images/staff/{{$item->thumbnails}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="banner_content">
                            <div class="mt-5"></div>
                            <h2>{{$item->last_name}} {{$item->first_name}}</h2>
{{--                            <p>Bạn sẽ bắt đầu nhận ra lý do tại sao bài tập này được gọi là Mô hình Dickens (có liên quan đến chương trình ma Scrooge một số tương lai khác nhau)</p>--}}
                            @if($attendance)
                            <a class="banner_btn" href="#">Ngày làm việc gần nhất: <b>{{$attendance->date_absent}}</b></a>
                            @else
                            <a class="banner_btn" href="#">Ngày làm việc gần nhất: <b>Hiện đang cập nhật</b></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="welcome_area p_120">
        <div class="container">
            <div class="row welcome_inner">
                <div class="col-lg-6">
                    <div class="welcome_text">
                        <h4>Thông tin cá nhân</h4>
                        <p>Họ tên: {{$item->last_name}} {{$item->first_name}}</p>
                        <p>Ngày sinh: {{$item->birthday}}</p>
                        @if($item->sex == 0)
                        <p>Giới tính: Nữ</p>
                        @else
                        <p>Giới tính: Nam</p>
                        @endif
                        <p>Địa chỉ Email: {{$item->email}}</p>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="wel_item">
                                    <i class="lnr lnr-database"></i>
                                    <h4>Phòng</h4>
                                    <p>{{$item->units['name']}}</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="wel_item">
                                    <i class="lnr lnr-book"></i>
                                    <h4>Tổ</h4>
                                    <p>{{$item->teams['name']}}</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="wel_item">
                                    <i class="lnr lnr-users"></i>
                                    <h4>Đảng</h4>
                                    <p>{{$item->date_clan}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="tools_expert">
                        <h3>Kinh nghiệm</h3>
                        <p>Trình độ: {{$item->level}}</p>
                        <p>Chức vụ: {{$item->position_staff}}</p>
                        <p>Thuộc cán bộ: {{$item->type_staff}}</p>
{{--                        <div class="skill_main">--}}
{{--                            <div class="skill_item">--}}
{{--                                <h4>After Effects <span class="counter">85</span>%</h4>--}}
{{--                                <div class="progress">--}}
{{--                                    <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="skill_item">--}}
{{--                                <h4>Photoshop <span class="counter">90</span>%</h4>--}}
{{--                                <div class="progress">--}}
{{--                                    <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="skill_item">--}}
{{--                                <h4>Illustrator <span class="counter">70</span>%</h4>--}}
{{--                                <div class="progress">--}}
{{--                                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="skill_item">--}}
{{--                                <h4>Sublime <span class="counter">95</span>%</h4>--}}
{{--                                <div class="progress">--}}
{{--                                    <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="skill_item">--}}
{{--                                <h4>Sketch <span class="counter">75</span>%</h4>--}}
{{--                                <div class="progress">--}}
{{--                                    <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection