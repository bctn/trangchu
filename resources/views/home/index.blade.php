@extends('layouts.basic')
@section('content')
    <!--================Home Banner Area =================-->
    <section class="home_banner_area">
        <div class="banner_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="home_left_img">
                            <img src="/logo/ubnd.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="banner_content">
                            <h5>Đây là hệ thống</h5>
                            <h2>hỗ trợ quản lý cán bộ xã Tân Tiến</h2>
                            <p>Năng động - Thân thiện - <strong>Chuyên nghiệp</strong> - Trách nhiệm - Hiện đại</p>
                            <a class="banner_btn" href="{{route('unit.index')}}">Khám phá ngay</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->
    <!--================Projects Area =================-->
    <!-- <section class="projects_area p_120">
        <div class="container">
            <div class="main_title">
                <h2>Cập nhật thông tin cán bộ</h2>
                <p>Xây dựng bộ máy cán bộ thực hiện nghiêm chỉnh chấp hành vì nước, vì dân</p>
            </div>
            <div class="projects_inner row">
                @foreach($staff as $item)
                    <div class="col-lg-4 col-sm-6 brand web">
                        <div class="projects_item">
                            <img class="img-fluid" src="http://127.0.0.1:8000/images/staff/{{$item->thumbnails}}" alt="">
                            <div class="projects_text">
                                <a href="{{route('staff.detail', ['slug' => $item->slug, 'id' => $item->id])}}"><h4>{{$item->first_name}} {{$item->last_name}}</h4></a>
                                <p>{{$item->code_number_staff}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section> -->
    <!--================End Projects Area =================-->

    <!--================Latest Blog Area =================-->
    <section class="latest_blog_area p_120">
        <div class="container">
            <div class="main_title">
                <h2>Bài Viết Mới Nhất</h2>
                <p>Xây dựng lên những thông tin bổ ích từ việc ra quyết dịnh của các cơ quan tổ chức cán bộ xã.</p>
            </div>
            <div class="row latest_blog_inner">
                @foreach($news as $item)
                <div class="col-lg-4">
                    <div class="l_blog_item">
                        <div class="l_blog_img">
                            <img class="img-fluid" src="http://127.0.0.1:8000/images/news/{{$item->thumbnails}}" alt="">
                        </div>
                        <div class="l_blog_text">
                            <div class="date">
                                <a href="#">{{$item->created_at}}  |  By Quản trị viên</a>
                            </div>
                            <a href="{{route('news.detail', ['slug' => $item->slug, 'id' => $item->id])}}"><h4>{{$item->title}}</h4></a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--================End Latest Blog Area =================-->

@endsection