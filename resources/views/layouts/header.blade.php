<header class="header_area">
    <div class="main_menu" id="mainNav">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container box_1620">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="/"><img src="/logo/home.png" width="70px" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="nav-item"><a class="nav-link" href="{{route('unit.index')}}">Phòng ban</a></li>
                        <!-- <li class="nav-item"><a class="nav-link" href="{{route('staff.index')}}">Cán bộ</a></li> -->
                        <li class="nav-item"><a class="nav-link" href="{{route('procedure.index')}}">Quy trình thủ tục</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('news.index')}}">Tin tức</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('contact.index')}}">Về chúng tôi</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>