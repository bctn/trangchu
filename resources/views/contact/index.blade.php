@extends('layouts.basic')
@section('content')
    <!--================Contact Area =================-->
    <section class="contact_area p_120">
        <div class="container">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12325.88407297558!2d107.82218164897111!3d10.71121193339443!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175ce6e97995b5f%3A0x257b8f66e3348c16!2zVUJORCBYw6MgVMOibiBUaeG6v24!5e1!3m2!1svi!2s!4v1589129556037!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            <div class="row mt-5">
                <div class="col-lg-3">
                    <div class="contact_info">
                        <div class="info_item">
                            <i class="lnr lnr-home"></i>
                            <h6>Xã Tân Tiến, Lagi, Bình Thuận</h6>
                            <p>Ủy ban Nhân Dân</p>
                        </div>
                        <div class="info_item">
                            <i class="lnr lnr-phone-handset"></i>
                            <h6><a href="#">00 (440) 9865 562</a></h6>
                            <p>Hoạt động từ T2-T7</p>
                        </div>
                        <div class="info_item">
                            <i class="lnr lnr-envelope"></i>
                            <h6><a href="#">support@gmail.com</a></h6>
                            <p>Gửi cho chúng tôi về truy vấn của bạn bất cứ lúc nào!</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    @if(Session::has('customer'))
                        <div class="alert alert-{{Session::get('customer')}} text-center" role="alert">
                            <b>{{Session::get('customer-notification')}}</b>
                        </div>
                    @endif
                    @if(count($errors) > 0)
                        @foreach($errors->all() as $err)
                                <div class="alert alert-danger text-center" role="alert">
                                    <b>{{$err}}</b>
                                </div>
                        @endforeach
                    @endif
                    <form class="row contact_form" action="{{route('contact.post')}}" method="post" id="contactForm"
                          novalidate="novalidate">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="full_name" name="full_name"
                                       placeholder="Vui lòng nhập họ tên của bạn...">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Vui lòng nhập email của bạn...">
                            </div>
                            <div class="form-group" style="float: left;">
                                <select class="form-control" id="sex" name="sex">
                                    <option value="0">Nữ</option>
                                    <option value="1">Nam</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="mobile" name="mobile"
                                       placeholder="Vui lòng nhập số điện thoại của bạn...">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" name="description" id="description" rows="2"
                                          placeholder="Nội dung phản hồi - đóng góp ý của bạn..."></textarea>
                            </div>
                        </div>
                        <div class="col-md-12 text-right">
                            <button type="submit" value="submit" class="btn submit_btn">Gửi</button>
                        </div>
                        {{csrf_field()}}
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!--================Contact Area =================-->
@endsection