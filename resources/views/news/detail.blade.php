@extends('layouts.basic')
@section('content')
    <!--================Portfolio Details Area =================-->
    <section class="portfolio_details_area p_120">
        <div class="container">
            <div class="portfolio_details_inner">
                <div class="row">
                    <div class="col-md-6">
                        <div class="left_img">
                            <img class="img-fluid" src="http://127.0.0.1:8000/images/news/{{$item->thumbnails}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portfolio_right_text">
                            <h4>{{$item->title}}</h4>
                            <ul class="list">
                                <li><span>Danh mục tin</span>:  {{$item->category_id}}</li>
                                <li><span>Nguồn</span>:  {{$item->source}}</li>
                                <li><span>Ngày đăng</span>:  {{$item->created_at}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <p>{!! $item->content  !!}</p>
            </div>
        </div>
    </section>
    <!--================End Portfolio Details Area =================-->
@endsection