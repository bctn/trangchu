@extends('layouts.basic')
@section('content')
    <section class="latest_blog_area p_120">
        <div class="container">
            <div class="main_title">
                <h2>Bài Viết Mới Nhất</h2>
                <p>Xây dựng lên những thông tin bổ ích từ việc ra quyết dịnh của các cơ quan tổ chức cán bộ xã.</p>
            </div>
            <div class="row latest_blog_inner">
                @foreach($news as $item)
                    <div class="col-lg-4">
                        <div class="l_blog_item">
                            <div class="l_blog_img">
                                <img class="img-fluid" src="http://127.0.0.1:8000/images/news/{{$item->thumbnails}}" alt="">
                            </div>
                            <div class="l_blog_text">
                                <div class="date">
                                    <a href="#">{{$item->created_at}}  |  By Quản trị viên</a>
                                </div>
                                <a href="{{route('news.detail', ['slug' => $item->slug, 'id' => $item->id])}}"><h4>{{$item->title}}</h4></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection