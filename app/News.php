<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";

    protected $fillable = [
        'title',
        'slug',
        'summary',
        'content',
        'content_type',
        'category_id',
        'thumbnails',
        'source',
        'start_date',
        'end_date',
    ];

    protected $filter = [
        'id',
        'title',
        'slug',
        'summary',
        'content',
        'content_type',
        'category_id',
        'thumbnails',
        'source',
        'start_date',
        'end_date',
    ];
}
