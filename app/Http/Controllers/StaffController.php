<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Staff;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    public function index()
    {
        $staff = Staff::all();
        return view('staff.index', [
            'staff' => $staff
        ]);
    }

    public function detail($slug, $id)
    {
        $staff = Staff::query()->with('teams', 'units')->findOrFail($id);

        $attendance = Attendance::query()
            ->where('code_number_staff', $staff->code_number_staff)
            ->where('absent', 0)
            ->orderByDesc('date_absent')
            ->first();

        return view('staff.detail', [
            'item' => $staff,
            'attendance' => $attendance,
        ]);
    }
}
