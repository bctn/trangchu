<?php

namespace App\Http\Controllers;

use App\News;
use App\Staff;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $news = News::query()->take(3)->get();

        $staff = Staff::query()->take(6)->get();

        return view('home.index', [
            'news'=>$news,
            'staff'=>$staff
        ]);
    }
}
