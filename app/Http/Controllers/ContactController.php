<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact.index');
    }

    public function postContact(Request $request)
    {
        $this->validate($request,
            [
                'full_name'                         => 'required',
                'email'                             => 'required',
                'sex'                               => 'required',
                'mobile'                            => 'required',
                'description'                       => 'required',
            ],
            [
                'full_name.required'                => 'Vui lòng nhập họ tên!',
                'email.required'                    => 'Vui lòng nhập email',
                'sex.required'                      => 'Vui lòng nhập giới tính',
                'mobile.required'                   => 'Vui lòng nhập số điện thoại',
                'description.required'              => 'Vui lòng nhập nội dung',
            ]
        );

        $payload = [];

        $payload['full_name']                               = $request->full_name;
        $payload['email']                                   = $request->email;
        $payload['sex']                                     = $request->sex;
        $payload['mobile']                                  = $request->mobile;
        $payload['description']                             = $request->description;

        // Tạo và lưu
        $contact = Contact::create($payload);
        DB::beginTransaction();


        try {
            $contact->save();
            DB::commit();
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
        }
        return redirect()->back()->with(['customer'=>'success','customer-notification'=>'Gửi yêu cầu thành công!']);
    }
}
