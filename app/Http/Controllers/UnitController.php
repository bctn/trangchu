<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use App\Staff;

class UnitController extends Controller
{
    public function index()
    {
        $unit = Unit::all();

        return view('unit.index', [
            'unit' => $unit
        ]);
    }

    public function detail($id)
    {
        $staff = Staff::query()->with('teams', 'units')->where('code_number_unit', $id)->get();

        return view('unit.detail', [
            'staff' => $staff
        ]);
    }
}
