<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::all();
        return view('news.index', [
            'news' => $news
        ]);
    }

    public function detail($slug, $id)
    {
        $news = news::query()->findOrFail($id);
        $similar_data =  News::query()->where('id', '!=' , $id)->take(4)->get();

        return view('news.detail', [
            'item' => $news,
            'similar_data' => $similar_data
        ]);
    }
}
