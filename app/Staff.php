<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = "staffs";

    public function teams()
    {
        return $this->belongsTo(Team::class, 'code_number_team', 'code_number_team');
    }

    public function units()
    {
        return $this->belongsTo(Unit::class, 'code_number_unit', 'code_number_unit');
    }
}
